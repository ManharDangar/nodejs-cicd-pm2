const express = require("express");
const app = express();

app.get("/", (req, res) => {
  res.send({ hello: "Hello from Production server" });
});

const PORT = process.env.PORT || 5000;
app.listen(PORT, function() {
  console.log(`App listening on port ${PORT}`);
});
 
